<?php


namespace GetNoticed\HomeBlocks\ViewModel;

use GetNoticed\HomeBlocks\Helper\Config\General;
use Magento\Framework\ {
    View\Element\Block\ArgumentInterface,
    App\Config\ScopeConfigInterface,
    UrlInterface
};
use Magento\Store\Model\StoreManagerInterface;

class SubBlock implements ArgumentInterface
{

    /**
     * @var General
     */
    private $helper;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        General $helper,
        StoreManagerInterface $storeManager
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->helper = $helper;
        $this->storeManager = $storeManager;
    }

    public function getImagePath($blockId): ?string
    {
        $image = $this->helper->getConfigValue('subblock'.$blockId.'/image');
        if ($image == '')
        {
            return null;
        }
        $path = $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'homeblocks/';
        return $path . $image;
    }

    public function getLink($blockId): ?string
    {
        return $this->helper->getConfigValue('subblock'.$blockId.'/link');
    }

    public function hasHover($blockId): bool
    {
        if ($this->helper->getConfigValue('subblock'.$blockId.'/hover') == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function getText($blockId): ?string
    {
        return $this->helper->getConfigValue('subblock'.$blockId.'/text');
    }

    public function getLabel($blockId): ?string
    {
        return $this->helper->getConfigValue('subblock'.$blockId.'/labeltext');
    }

    /**
     * @return bool
     */
    public function needsDisplaying()
    {
        if (!$this->helper->isModuleEnabled() ){
            return false;
        }
        if (!$this->getText(1) || !$this->getText(2) || !$this->getText(3)) {
            return false;
        }

        if (!$this->getImagePath(1) || !$this->getImagePath(2) || !$this->getImagePath(3)) {
            return false;
        }

        return true;
    }

}