<?php


namespace GetNoticed\HomeBlocks\ViewModel;

use GetNoticed\HomeBlocks\Helper\Config\General;
use Magento\Framework\ {
    View\Element\Block\ArgumentInterface,
    App\Config\ScopeConfigInterface,
    UrlInterface
};
use Magento\Store\Model\StoreManagerInterface;

class MainBlock implements ArgumentInterface
{

    /**
     * @var General
     */
    private $helper;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        General $helper,
        StoreManagerInterface $storeManager
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->helper = $helper;
        $this->storeManager = $storeManager;
    }

    public function getImagePath(): ?string
    {
        $image = $this->helper->getConfigValue('mainblock/image');
        if ($image == '')
        {
            return null;
        }
        $path = $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'homeblocks/';
        return $path . $image;
    }


    public function getText(): ?string
    {
        return $this->helper->getConfigValue('mainblock/text');
    }

    /**
     * @return bool
     */
    public function needsDisplaying()
    {
        if (!$this->helper->isModuleEnabled() ){
            return false;
        }
        if (!$this->getText()) {
            return false;
        }
        if (!$this->getImagePath()) {
            return false;
        }

        return true;
    }


}