<?php

namespace GetNoticed\HomeBlocks\Helper\Config;

interface GeneralInterface
{

    const XML_PATH_BASE = 'getnoticed_homeblocks';
    const XML_PATH_ENABLED = 'general/enabled';

    public function isModuleEnabled(): bool;

    public function getConfigValue($key): ?string;

}