<?php

namespace GetNoticed\HomeBlocks\Helper\Config;

use Magento\Store;

class General extends AbstractConfigHelper implements GeneralInterface
{

    public function isModuleEnabled(): bool
    {
	return ($this->getConfigValue(self::XML_PATH_ENABLED)==1?true:false);
    }

    public function getConfigValue($key): ?string
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_BASE . '/' . $key,
            Store\Model\ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore());
    }
    
}
